from fastapi import FastAPI
from joblib import load
from pydantic import BaseModel
from typing import List, Dict
from numpy import array
from data_preprocessing import normalize, features, find_first_name, is_multiple_words
from names_dataset import NameDataset
import os
from functools import partial

app = FastAPI(debug=True)
model = load("decision_tree_clf.joblib")
vectorizer = load("vectorizer.joblib")
mapping = {True: "F", False: "M"}


def _reload_dataset():
    dataset.first_names_filename = os.path.join(
        os.path.dirname(__file__), "fnames_db.txt"
    )
    with open(dataset.first_names_filename, "r", errors="ignore", encoding="utf8") as r:
        dataset.first_names = set(r.read().strip().split("\n"))


dataset = NameDataset()
_reload_dataset()
find_first_name = partial(find_first_name, dataset=dataset, return_none=False)


class PredictionRequest(BaseModel):
    names: List[str]


class SingleName(BaseModel):
    processed_name: str
    gender: str


class PredictionResponse(BaseModel):
    results: Dict[str, SingleName]


@app.post(
    "/predict",
    response_model=PredictionResponse,
    description="Multiple names prediction",
    name="Multi prediction",
)
async def predict(pred_request: PredictionRequest):
    valid_names, invalid_names = [], []
    for name in pred_request.names:
        processed = normalize(name)
        if processed:
            valid_names.append(processed)
        else:
            invalid_names.append(name)

    valid_names = [
        find_first_name(name) if is_multiple_words(name) else name
        for name in valid_names
    ]

    valid_results = {}
    if valid_names:
        name_features = features(valid_names)
        transformed = vectorizer.transform(array(name_features))
        predictions = [mapping[pred] for pred in model.predict(transformed)]
        original_names = [
            name for name in pred_request.names if name not in invalid_names
        ]

        valid_results = {
            original_name: SingleName(processed_name=processed_name, gender=gender)
            for original_name, processed_name, gender in zip(
                original_names, valid_names, predictions
            )
        }

    invalid_results = {
        original_name: SingleName(processed_name=original_name, gender="U")
        for original_name in invalid_names
    }

    valid_results.update(invalid_results)

    return PredictionResponse(results=valid_results)


@app.post(
    "/spredict",
    response_model=SingleName,
    description="Single name prediction",
    name="Single prediction",
)
async def spredict(name: str):
    proc_name = normalize(name)
    if not proc_name:
        return SingleName(processed_name=name, gender="U")

    fname = find_first_name(proc_name)
    if not fname:
        return SingleName(processed_name=name, gender="U")

    name_features = features(fname).tolist()
    transformed = vectorizer.transform(name_features)
    prediction = model.predict(transformed)[0]
    gender = mapping[prediction]

    return SingleName(processed_name=fname, gender=gender)
