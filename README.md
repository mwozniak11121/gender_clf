**Python >= 3.6**
```
pip install -r requirements.txt
uvicorn web_service:app
```

Used [WGND](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/YPRQH8) dataset.
Data used is in `pd.DataFrame` format only with two columns: `name` and `gender`.
Any gender not in `{'M', 'F'}` was dropped.
Same applies to empty names.
Uses some external dataset (`names-dataset`) package to find first names.

Data was fetched from file `wgnd_ctry.csv` placed in dir `wgnd`.
(So path looks like this:
`os.path.join(os.getcwd(), "wgnd", "wgnd_ctry.csv")`
)

`data_processing.dataset_pipeline()` generates `./processed_data.csv'` file, which is later
used by `classification_tree.create_and_train()`.

`create_and_train()` saves `DecisionTreeClassifier` and `DictVectorizer` object in local dir.
These files are later loaded at start of `FastApi` app.

To add any additional data you've gotta concat data files and guarantee the same format.
(Columns `gender` and `name`, `gender` is mapped to `bool` using
`{'M': False, 'F': True}`, and `name` column type is `str`.
)

Command for training model looks like this:

`python .\classification_tree.py $PATH_WITH_DATA`.