import os
from unidecode import unidecode
import pandas as pd
import numpy as np
from sklearn.feature_extraction import DictVectorizer
from sklearn.model_selection import train_test_split
from typing import Tuple
from string import digits
from names_dataset import NameDataset
import re

SYMBOLS = [
    "`",
    "~",
    "!",
    "@",
    "#",
    "$",
    "%",
    "^",
    "&",
    "*",
    "(",
    ")",
    "_",
    "-",
    "+",
    "=",
    "{",
    "[",
    "]",
    "}",
    "|",
    "\\",
    ":",
    ";",
    '"',
    "<",
    ",",
    ">",
    ".",
    "?",
    "/",
]
DIGIT_TABLE = str.maketrans(dict(zip(digits, [" "] * len(digits))))
SYMBOLS_TABLE = str.maketrans(dict(zip(SYMBOLS, [" "] * len(SYMBOLS))))
RE_PATTERNS = {
    "EMOJI": re.compile(r":\w*:"),
    "PREFIX": re.compile(r"\b(\w)\1+"),
    "POSTFIX": re.compile(r"(\w)\1+\b"),
}
REPLACEMENT = {"$": "s"}


def replace_chars(name: str):
    for k, v in REPLACEMENT.items():
        name = name.replace(k, v)
    return name


def normalize(name: str) -> str:
    decoded = unidecode(name.strip().lower())
    without_emojis = RE_PATTERNS["EMOJI"].sub("", decoded).strip()
    replaced = replace_chars(without_emojis)
    without_pre = RE_PATTERNS["PREFIX"].sub(r"\1", replaced)
    without_post = RE_PATTERNS["POSTFIX"].sub(r"\1", without_pre)
    without_digits = without_post.translate(DIGIT_TABLE)
    spaced = without_digits.translate(SYMBOLS_TABLE)
    return spaced.strip().lower()


def is_multiple_words(name: str) -> bool:
    return len(name.split()) > 1


def dataset_pipeline(data_dir: str):
    dfs = []
    for file in os.listdir(data_dir):
        file_path = os.path.join(data_dir, file)
        dfs.append(pd.read_csv(file_path, usecols=["name", "gender"]))

    data = pd.concat(dfs).reset_index(drop=True)

    data.gender = (
        data.gender.str.upper()
        .map({"F": True, "M": False, "f": True, "m": False})
        .astype(bool)
    )
    data.dropna(inplace=True)

    data.name = data.name.apply(normalize)

    data.to_csv("./processed_data.csv", index=False)


@np.vectorize
def features(name: str):
    return {
        "f1": name[0],
        "f2": name[:2],
        "f3": name[:3],
        "l1": name[-1],
        "l2": name[-2:],
        "l3": name[-3:],
        "len": len(name),
    }


def data_pipeline(
    test_size: float = None,
) -> Tuple[np.matrix, np.matrix, np.ndarray, np.ndarray, DictVectorizer]:
    dataset = NameDataset()
    data = pd.read_csv("./processed_data.csv")
    data.dropna(inplace=True)

    data["multiple_words"] = data.name.apply(is_multiple_words)

    multi_words_data = data.query("multiple_words")
    single_words_data = data.query("not multiple_words")

    multi_words_data.name = multi_words_data.name.apply(
        find_first_name, dataset=dataset
    )

    data = pd.concat([multi_words_data, single_words_data]).drop(
        "multiple_words", axis=1
    )
    data.drop_duplicates(subset=["name"], inplace=True)

    names = data.name.to_numpy(dtype=np.str)
    name_features = features(names)

    vectorizer = DictVectorizer()
    vectorizer.fit(name_features)

    X = vectorizer.transform(name_features)
    Y = data.gender.to_numpy(dtype=bool)

    if test_size:
        x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=test_size)
    else:
        x_train, x_test, y_train, y_test = X, None, Y, None

    return x_train, x_test, y_train, y_test, vectorizer


def find_first_name(name: str, dataset: NameDataset, return_none: bool = False) -> str:
    words = name.split()
    result = next((word for word in words if dataset.search_first_name(word)), "")
    if not result:
        if return_none:
            return ""
        else:
            return name
    return result
