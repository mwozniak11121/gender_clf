from sklearn.tree import DecisionTreeClassifier
from joblib import dump
from data_preprocessing import data_pipeline, dataset_pipeline
import sys


def create_and_train():
    x_train, x_test, y_train, y_test, vectorizer = data_pipeline()
    clf = DecisionTreeClassifier()

    clf.fit(x_train, y_train)

    dump(clf, "decision_tree_clf.joblib")
    dump(vectorizer, "vectorizer.joblib")


if __name__ == "__main__":
    dataset_pipeline(sys.argv[1])
    create_and_train()
